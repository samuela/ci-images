FROM debian:stretch

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV LANG C.UTF-8

RUN apt-get update -qq; apt-get install --no-install-recommends -qy gnupg dirmngr \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# http://downloads.haskell.org/debian/
RUN echo 'deb http://downloads.haskell.org/debian stretch main' > /etc/apt/sources.list.d/ghc.list
RUN apt-key adv --keyserver keyserver.ubuntu.com  --recv-keys BA3CBA3FFE22B574

# Core build utilities
RUN apt-get update -qq && apt-get install --no-install-recommends -qy \
    zlib1g-dev libtinfo-dev libsqlite3-0 libsqlite3-dev libgmp-dev \
    ca-certificates g++ git make automake autoconf gcc \
    perl python3 texinfo xz-utils pxz lbzip2 bzip2 patch openssh-client sudo time \
    jq wget curl \
    # For source distributions
    xutils-dev \
    # DWARF libraries
    libdw1 libdw-dev \
    # For nofib
    valgrind \
    # Documentation tools
    python3-sphinx texlive-xetex texlive-latex-extra texlive-binaries texlive-fonts-recommended lmodern texlive-generic-extra \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /tmp
# Install GHC and cabal
ENV GHC_VERSION 8.8.3
RUN curl -L https://downloads.haskell.org/~ghc/$GHC_VERSION/ghc-$GHC_VERSION-x86_64-deb9-linux.tar.xz | tar -Jx;
WORKDIR /tmp/ghc-$GHC_VERSION
RUN ./configure --prefix=/opt/ghc/$GHC_VERSION; \
    make install;

WORKDIR /tmp
RUN rm -rf /tmp/ghc-$GHC_VERSION
ENV PATH /opt/ghc/$GHC_VERSION/bin:$PATH

# Get Cabal
ENV CABAL_VERSION 3.0.0.0
RUN curl -L https://downloads.haskell.org/cabal/cabal-install-$CABAL_VERSION/cabal-install-$CABAL_VERSION-x86_64-unknown-linux.tar.xz | tar -Jx && \
    mv cabal /usr/local/bin/cabal

# LLVM
ENV LLVM_DIR /opt/llvm
ENV LLVM_VERSION 9.0.0
ENV PATH $LLVM_DIR/bin:$PATH
RUN curl -L http://releases.llvm.org/$LLVM_VERSION/clang+llvm-$LLVM_VERSION-x86_64-linux-gnu-ubuntu-16.04.tar.xz | tar -xJC .; \
    mkdir $LLVM_DIR && \
    cp -R clang+llvm*/* $LLVM_DIR && \
    rm -R clang+llvm* && \
    llc --version

# Create a normal user.
RUN adduser ghc --gecos "GHC builds" --disabled-password
RUN echo "ghc ALL = NOPASSWD : ALL" > /etc/sudoers.d/ghc
USER ghc
WORKDIR /home/ghc/

# Build Haskell tools
RUN cabal v2-update && \
    cabal v2-install hscolour happy alex --constraint 'happy ^>= 1.19.10'
ENV PATH /home/ghc/.cabal/bin:$PATH

CMD ["bash"]
