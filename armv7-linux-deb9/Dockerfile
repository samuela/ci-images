FROM arm32v7/debian:stretch

ENV LANG C.UTF-8

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Core build utilities
RUN apt-get update \
    && apt-get install --no-install-recommends -qy zlib1g-dev libtinfo-dev libgmp10 libgmp-dev \
    libsqlite3-0 libsqlite3-dev \
    ca-certificates g++ git make automake autoconf gcc \
    perl python3 texinfo xz-utils pxz lbzip2 bzip2 patch openssh-client sudo time \
    jq wget curl \
    # Documentation tools
    python3-sphinx texlive-xetex texlive-latex-extra texlive-binaries texlive-fonts-recommended lmodern texlive-generic-extra \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Boot LLVM
ENV BOOT_LLVM_DIR /opt/llvm-bootstrap
ENV BOOT_LLVM_VERSION 7.0.0
ENV PATH /usr/local/bin:$PATH
RUN curl -L http://releases.llvm.org/$BOOT_LLVM_VERSION/clang+llvm-$BOOT_LLVM_VERSION-armv7a-linux-gnueabihf.tar.xz | tar -xJC . && \
    mkdir $BOOT_LLVM_DIR && \
    cp -R clang+llvm*/* $BOOT_LLVM_DIR && \
    rm -R clang+llvm* && \
    $BOOT_LLVM_DIR/bin/llc --version

# GHC
ENV GHC_VERSION 8.8.1
RUN curl -L https://downloads.haskell.org/ghc/$GHC_VERSION/ghc-$GHC_VERSION-armv7l-deb9-linux.tar.xz | tar -xJ
WORKDIR /ghc-$GHC_VERSION
RUN ./configure --prefix=/usr/local LLC=$BOOT_LLVM_DIR/bin/llc OPT=$BOOT_LLVM_DIR/bin/opt && \
    make install
WORKDIR /
RUN rm -Rf ghc-*
RUN ghc --version

# LLVM
ENV LLVM_DIR /opt/llvm
ENV LLVM_VERSION 9.0.0
ENV PATH $LLVM_DIR/bin:$PATH
RUN curl -L http://releases.llvm.org/$LLVM_VERSION/clang+llvm-$LLVM_VERSION-armv7a-linux-gnueabihf.tar.xz | tar -xJC . && \
    mkdir $LLVM_DIR && \
    cp -R clang+llvm*/* $LLVM_DIR && \
    rm -R clang+llvm* && \
    llc --version

# Cabal
ENV CABAL_VERSION 3.0.0.0
RUN curl -L http://home.smart-cactus.org/~ben/ghc/cabal-install-$CABAL_VERSION-armv7l-debian9-linux.tar.xz | tar -Jx && \
    mv cabal /usr/local/bin/cabal

ENV PATH /home/ghc/.local/bin:/opt/ghc/$GHC_VERSION/bin:$PATH

# Create a normal user.
RUN adduser ghc --gecos "GHC builds" --disabled-password
RUN echo "ghc ALL = NOPASSWD : ALL" > /etc/sudoers.d/ghc
USER ghc
WORKDIR /home/ghc/

# Build Haskell tools
RUN cabal update && \
    cabal install hscolour happy alex --constraint 'happy ^>= 1.19.10'
ENV PATH /home/ghc/.cabal/bin:$PATH

CMD ["bash"]

